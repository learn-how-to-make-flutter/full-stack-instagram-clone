import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:instagram_flutter/responsive/mobile_scree_layout.dart';
import 'package:instagram_flutter/responsive/responsive_layout_srenn.dart';
import 'package:instagram_flutter/responsive/web_scree_layout.dart';
import 'package:instagram_flutter/screens/login_screens.dart';
import 'package:instagram_flutter/screens/signup_screens.dart';
import 'package:instagram_flutter/utils/colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (kIsWeb) {
    await Firebase.initializeApp(
      options: const FirebaseOptions(
        apiKey: "AIzaSyApJRxhYnxNs11j_NW5AIgv3YB2BG9zdHM",
        appId: "1:820990908866:web:a0f5dc6964561b8d309e5b",
        messagingSenderId: "820990908866",
        projectId: "instagram-ed",
        storageBucket: "instagram-ed.appspot.com",
      ),
    );
  } else {
    await Firebase.initializeApp();
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Instagram Clone',
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: mobileBackgroundColor,
      ),
      // home: const ReponsiveLayout(
      //   moblieScreenLayout: MobileScreenLayout(),
      //   webScreenLayout: WebScreenLayout(),
      // ),
      home: SignupScreen(),
    );
  }
}
